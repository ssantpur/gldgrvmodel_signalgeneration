#--------------------------------------------------------------
# MC15 common configuration
#--------------------------------------------------------------

include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")
include("MC15JobOptions/Pythia8_EvtGen.py")


#include("MC15JobOptions/Pythia8_Photos.py")
#comment out the photos option, see reason below
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasMcProductionMC15#Use_of_Photos_Tauola_and_EvtGen

#--------------------------------------------------------------
# Configuration for Gldgrv model
#--------------------------------------------------------------
evgenConfig.description = "MG5 to Pythia8 and EvtGen"
evgenConfig.keywords = ["longLived"]
evgenConfig.generators += ["Lhef","aMcAtNlo"]
evgenConfig.inputfilecheck = "C1N2"
